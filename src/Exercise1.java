import java.util.Scanner;

public class Exercise1 {
    static void order(String food){
        System.out.println("We have received your order for "+food+"! It will be served soon!");
    }
    public static void main(String[] args) {
        Scanner userIn = new Scanner(System.in);
        System.out.println("What would you like to order:");
        System.out.println("1. Tempura");
        System.out.println("2. Ramen");
        System.out.println("3. Udon");
        System.out.print("Your order: ");
        int orderNum=userIn.nextInt();
        userIn.close();
        if(orderNum==1){
            order("Tempura");
        }
        else if(orderNum==2){
            order("Ramen");
        }
        else if(orderNum==3){
            order("Udon");
        }
    }
}
